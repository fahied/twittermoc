//
//  AppDelegate.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readonly)CoreDataHelper *coreDataHelper;

//shared flag hold the network reachablity status
@property(assign) BOOL isReachable;


@end

