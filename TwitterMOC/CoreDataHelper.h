//
//  CoreDataHelper.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>



@interface CoreDataHelper : NSObject

//public properties
@property (nonatomic,readonly) NSManagedObjectContext           *parentContext;
@property (nonatomic,readonly) NSManagedObjectContext           *context;

@property (nonatomic,readonly) NSManagedObjectModel              *model;
@property (nonatomic,readonly) NSPersistentStoreCoordinator      *coordinator;
@property (nonatomic,readonly) NSPersistentStore                 *store;

//public methods
-(void)setupCoreData;
-(void)saveContext;
-(void)backgroundSaveContext;

@end
