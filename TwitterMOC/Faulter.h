//
//  Faulter.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>



@interface Faulter : NSObject

//put an entity to fault state to save momery
+(void)faultObjectWithID:(NSManagedObjectID*)objectID inContext:(NSManagedObjectContext*)context;

@end
