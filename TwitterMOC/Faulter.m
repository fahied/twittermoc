//
//  Faulter.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "Faulter.h"

@implementation Faulter


+(void)faultObjectWithID:(NSManagedObjectID*)objectID inContext:(NSManagedObjectContext*)context
{
    if (!objectID|| !context) {
        return;
    }
    
    [context performBlockAndWait:^{
        NSManagedObject *object = [context objectWithID:objectID];
        if (object.hasChanges) {
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Error saving context: %@",[error localizedDescription]);
            }
        }
        
        if (object.isFault) {
            NSLog(@"Faulting object: %@",object.objectID);
            [context refreshObject:object mergeChanges:NO];
        } else
        {
            NSLog(@"Skipp faulting as object is already a fault.");
        }
        // Repeat the process if context has a parent
        if (context.parentContext) {
            [self faultObjectWithID:objectID inContext:context.parentContext];
        }
    }];
}
@end
