//
//  NSDate+Difference.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Difference)

+ (NSString *)dateDiff:(NSDate *)date;

@end
