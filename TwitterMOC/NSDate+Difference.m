//
//  NSDate+Difference.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "NSDate+Difference.h"

@implementation NSDate (Difference)


// calculae the rounded time interval passes from now to date
+ (NSString *)dateDiff:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
    
    NSString *resultString = [formatter stringFromDate:date];
    NSDate *convertedDate = [formatter dateFromString:resultString];
    
    NSDate *todayDate = [NSDate date];
    double ti = [convertedDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    NSInteger diff = round(ti / 60);
    
    if (ti < 60) {
        return @"a minute ago";
    } else if (ti < 3600) {
        return [NSString stringWithFormat:@"%ld minutes ago", (long)diff];
    } else if (ti < 86400) {
        diff = round(ti / 60 / 60);
        return[NSString stringWithFormat:@"%ld hours ago", (long)diff];
    } else if (ti < 604800) {
        diff = round(ti / 60 / 60 / 24);
        return[NSString stringWithFormat:@"%ld days ago", (long)diff];
    } else if (ti < 2678400) {
        diff = round(ti / 60 / 60 / 24 / 7);
        return[NSString stringWithFormat:@"%ld weeks ago", (long)diff];
    } else if (ti < 31536000) {
        diff = round(ti / 60 / 60 / 24 / 31);
        return[NSString stringWithFormat:@"%ld months ago", (long)diff];
    } else if (ti >= 2678400) {
        diff = round(ti / 60 / 60 / 24 / 365);
        return[NSString stringWithFormat:@"%ld years ago", (long)diff];
    } else {
        return @"never";
    }
}



@end
