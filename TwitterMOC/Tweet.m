//
//  Tweet.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "Tweet.h"
#import "User.h"


@implementation Tweet

@dynamic created_at;
@dynamic id;
@dynamic text;
@dynamic user;

@end
