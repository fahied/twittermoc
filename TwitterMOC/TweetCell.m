//
//  TweetCell.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "TweetCell.h"
#import "User.h"
#import "NSDate+Difference.h"


#import <UIKit+AFNetworking.h>


@implementation TweetCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureCellWithTweet:(Tweet*)tweet
{
    self.userNameLabel.text = tweet.user.name;
    self.tweetTextLabel.text = tweet.text;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEE MMM dd HH:mm:ss Z yyyy"];
    NSDate *date = [dateFormatter dateFromString:tweet.created_at];
    
    self.timeLabel.text = [NSDate dateDiff:date];
    [self.backgroundImageView setImage:[[UIImage imageNamed:@"tweetItemBkg"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    //save ourself from retain cycle
    __weak typeof (self) weekself = self;
    [weekself.profileImageView setImageWithURL:[NSURL URLWithString:tweet.user.profile_image_url_https] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
}


+(CGFloat)heightForRowForText:(NSString*)tweetText
{
    
    CGSize expectedLabelSize = [tweetText boundingRectWithSize: CGSizeMake(296, FLT_MAX)
                                              options: NSStringDrawingUsesLineFragmentOrigin
                                             attributes: @{NSFontAttributeName:[UIFont systemFontOfSize:15.0]}
                                              context: nil].size;
    
    return fmaxf(70.0f,(expectedLabelSize.height+10)+30);
}




@end
