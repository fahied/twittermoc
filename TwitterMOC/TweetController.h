//
//  TwitterController.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TweetController : NSObject

+(id)sharedInstance;


//complection block return YES if the operation completed successfully
typedef void (^TComplectionHandler)(BOOL success);
-(void)importRecentTimeLineWithCompletion:(TComplectionHandler)completion;


@end
