//
//  TwitterController.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "TweetController.h"
#import "TwitterClient.h"
#import "AppDelegate.h"
#import "Faulter.h"


//managed classes
#import "User.h"
#import "Tweet.h"




@implementation TweetController

#pragma mark -Singleton
+(id)sharedInstance
{
    static TweetController *tweetMyController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tweetMyController = [[self alloc]init];
    });
    return tweetMyController;
}



# pragma mark - import twitter time line to core data
-(void)importRecentTimeLineWithCompletion:(TComplectionHandler)completion
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    
    [[TwitterClient sharedInstance]configureTwitterClientWithHandler:^(BOOL configured) {
        if (configured) {
            [[TwitterClient sharedInstance]fetchMyHomeTimelineWithHandler:^(NSArray *results, NSError *error) {
                if (!error) {
                    NSLog(@"%lu tweets fetched",(unsigned long)results.count);
                    
                    if (results.count ==  0) {
                        return;
                    }
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        //Call your function or whatever work that needs to be done
                        //Code in this part is run on a background thread
                        [self parseAndSaveTweets:results];
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            
                            //Stop your activity indicator or anything else with the GUI
                            //Code here is run on the main thread
                            completion(YES);
                        });
                    });
                    
                }
            }];
        }
    }];
}


-(void)parseAndSaveTweets:(NSArray*)tweets
{
    // get handle to shared NSManagedObjectContext
    NSManagedObjectContext *context = [[(AppDelegate*)[[UIApplication sharedApplication] delegate] coreDataHelper]context];
    
    [context performBlock:^{
        
        for (NSDictionary *tweet in tweets) {
            
            //use autorelease pool to free memory immediately after intialzing object in a loop
            @autoreleasepool {
                
                // Step 1: find if the same record already exist
                Tweet *tweetEntity = (Tweet*)[self findRecordOfType:@"Tweet" withID:tweet[@"id"] inContext:context];
                
                //if found skip to next tweet
                if (tweetEntity) {
                    continue;
                }
                else
                {
                    // Step 2: Insert Tweet
                    tweetEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Tweet" inManagedObjectContext:context];
                    
                    //determine what information we want to collect
                    NSDictionary *tweetAttributes = [[tweetEntity entity]attributesByName];
                    
                    //loop through the required attributs and fill in values from tweet dictionary
                    for (NSString *attribute  in tweetAttributes) {
                        id value = [tweet objectForKey:attribute];
                        //if value is nill then skip to next attribut
                        if (value == nil || value == [NSNull null]) {
                            continue;
                        }
                        [tweetEntity setValue:value forKey:attribute];
                    }
                }

                // repeate Step1 and Step 2 for User record as well
                User *userEntity = (User*)[self findRecordOfType:@"User" withID:[tweet[@"user"] objectForKey:@"id"] inContext:context];
                //if found skip to next tweet
                if (!userEntity) {
                    // Step 2: Insert Tweet
                    userEntity = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                    
                    //determine what information we want to collect
                    NSDictionary *userAttributes = [[userEntity entity]attributesByName];
                    
                    //loop through the required attributs and fill in values from tweet dictionary
                    for (NSString *attribute  in userAttributes) {
                        id value = [tweet[@"user"] objectForKey:attribute];
                        //if value is nill then skip to next attribut
                        if (value == nil || value == [NSNull null]) {
                            continue;
                        }
                        [userEntity setValue:value forKey:attribute];
                    }
                }
                
                // Step 3: add relationships
                [userEntity addTweetsObject:tweetEntity];
                tweetEntity.user = userEntity;
                
                // Step 4: Turn object into fault to save memory
                [Faulter faultObjectWithID:tweetEntity.objectID inContext:context];
            }
            // Step : save new object to persistance store
            //[[(AppDelegate*)[[UIApplication sharedApplication] delegate] coreDataHelper]saveContext];
            [context reset];
        }
        // send notifications
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewTweetFound" object:self];
    }];
    
}


// helper method to find existing objects
-(NSManagedObject*)findRecordOfType:(NSString*)type withID:(NSString*)objectID inContext:(NSManagedObjectContext*)context
{
    NSManagedObject *record = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    request.entity = [NSEntityDescription entityForName:type inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"id == %@",objectID];
    
    NSError *error = nil;
    record = [[context executeFetchRequest:request error:&error]lastObject];
    return record;
    
}



@end
