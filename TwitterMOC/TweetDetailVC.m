//
//  DetailViewController.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "TweetDetailVC.h"

@interface TweetDetailVC ()

@end

@implementation TweetDetailVC

#pragma mark - Managing the detail item

- (void)setTweet:(Tweet*)newDetailItem {
    if (_tweet != newDetailItem) {
        _tweet = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.tweet) {
        self.detailDescriptionLabel.text = [self.tweet text];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
