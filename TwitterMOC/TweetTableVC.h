//
//  MasterViewController.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"


@class TweetDetailVC;


@interface TweetTableVC : UITableViewController <NSFetchedResultsControllerDelegate, UISearchBarDelegate>

@property (strong, nonatomic) TweetDetailVC *tweetDetailVC;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;


//core data
@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, retain) NSManagedObjectContext *managedObjectContext;




@end

