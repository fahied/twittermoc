//
//  TwitterClient.h
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterClient : NSObject


+ (id)sharedInstance;

// complection block return if the twitter client is configured
typedef void(^configurationHandler)(BOOL configured);
-(void)configureTwitterClientWithHandler:(configurationHandler)configured;


// complection bock for fetching seults form Twitter REST API
typedef void (^TCComplectionHandler)(NSArray *results, NSError *error);
-(void)fetchMyHomeTimelineWithHandler:(TCComplectionHandler)complection;


@end
