//
//  TwitterClient.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "TwitterClient.h"
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

#define kTwitterAccountIdentifier @"kTwitterAccountIdentifier"



@interface TwitterClient()
{
    //priavte var
    ACAccount *twitterAccountStoredInDevice;
    
}

@end

@implementation TwitterClient


#pragma Signleton
+ (id)sharedInstance
{
    static TwitterClient *sharedMyClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyClient = [[self alloc]init];
    });
    return sharedMyClient;
}




-(void)configureTwitterClientWithHandler:(configurationHandler)configured
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    
    //if already configured exit from here
    if (twitterAccountStoredInDevice) {
        configured(YES);
    }
    
    //check if user have twitter account saved in device settings
    ACAccountStore *accountStore = [[ACAccountStore alloc]init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //request access to local twitter account
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            [[[UIAlertView alloc]initWithTitle:@"ERROR" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil]show];
            configured(NO);
        }
        
        if ([self session]) {
            twitterAccountStoredInDevice = [accountStore accountWithIdentifier:[self session]];
            configured(YES);
        }
        else
        {
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            if (accounts.count == 0) {
                [[[UIAlertView alloc]initWithTitle:@"ERROR" message:@"Please add a Twitter account in the device Settings" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil]show];
                configured(NO);
            }
            // if twitter account found, save it
            twitterAccountStoredInDevice = [accounts firstObject];
            [self storeSession:twitterAccountStoredInDevice];
            configured(YES);
        }
    }];
}


-(void)fetchMyHomeTimelineWithHandler:(TCComplectionHandler)complection
{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    
    NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1/statuses/home_timeline.json"];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"20" forKey:@"count"];
    [parameters setObject:@"0" forKey:@"include_entities"];
    [parameters setObject:@"0" forKey:@"contributor_details"];
    [parameters setObject:@"1" forKey:@"exclude_replies"];
    
    
    SLRequest *postRequest = [SLRequest
                              requestForServiceType:SLServiceTypeTwitter
                              requestMethod:SLRequestMethodGET
                              URL:requestURL parameters:parameters];
    
    postRequest.account = twitterAccountStoredInDevice;
    
    [postRequest performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse
       *urlResponse, NSError *error)
     {
         NSArray *dataSource = [NSJSONSerialization
                                JSONObjectWithData:responseData
                                options:NSJSONReadingMutableLeaves
                                error:&error];
         
         if (dataSource.count != 0) {
             complection(dataSource, nil);
         }
     }];
}

#pragma mark - Session mehtods
- (void)storeSession:(ACAccount *)account
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account.identifier forKey:kTwitterAccountIdentifier];
    [userDefaults synchronize];
}

- (NSString *)session
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:kTwitterAccountIdentifier];
}



@end
