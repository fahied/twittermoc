//
//  User.m
//  TwitterMOC
//
//  Created by Muhammad Fahied on 31/10/14.
//  Copyright (c) 2014 Muhammad Fahied. All rights reserved.
//

#import "User.h"
#import "Tweet.h"


@implementation User

@dynamic id;
@dynamic name;
@dynamic profile_image_url_https;
@dynamic screen_name;
@dynamic followers_count;
@dynamic tweets;

@end
